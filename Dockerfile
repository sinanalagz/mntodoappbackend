FROM openjdk:13-alpine
Volume /tmp
ADD /target/*.jar mntodoappbackend-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/mntodoappbackend-0.0.1-SNAPSHOT.jar"]