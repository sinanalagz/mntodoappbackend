package com.mn.todoapp.service;

import com.mn.todoapp.entity.Todo;

import java.util.List;

public interface TodoService {
    Todo saveTodo(Todo todo);

    List<Todo> getTodos();

    Todo updateTodo(Long todoId, boolean completed);
}
