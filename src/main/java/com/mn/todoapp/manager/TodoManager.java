package com.mn.todoapp.manager;

import com.mn.todoapp.dto.TodoSaveRequestDTO;
import com.mn.todoapp.dto.TodoUpdateRequestDTO;
import com.mn.todoapp.entity.Todo;

import java.util.List;

public interface TodoManager {
    Todo saveTodo(TodoSaveRequestDTO todoSaveRequestDTO);

    List<Todo> getTodos();

    Todo updateTodo(TodoUpdateRequestDTO todoUpdateRequestDTO);
}
