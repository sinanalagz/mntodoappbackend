package com.mn.todoapp.manager;

import com.mn.todoapp.dto.TodoSaveRequestDTO;
import com.mn.todoapp.dto.TodoUpdateRequestDTO;
import com.mn.todoapp.entity.Todo;
import com.mn.todoapp.service.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TodoManagerImpl implements TodoManager {
    private final TodoService todoService;

    @Override
    public Todo saveTodo(TodoSaveRequestDTO todoSaveRequestDTO) {
        Todo todo = Todo
                .builder()
                .task(todoSaveRequestDTO.getTask())
                .completed(todoSaveRequestDTO.isCompleted())
                .build();
        return todoService.saveTodo(todo);
    }

    @Override
    public List<Todo> getTodos() {
        return todoService.getTodos();
    }

    @Override
    public Todo updateTodo(TodoUpdateRequestDTO todoUpdateRequestDTO) {
        Long id = todoUpdateRequestDTO.getId();
        boolean completed = todoUpdateRequestDTO.isCompleted();
        return todoService.updateTodo(id, completed);
    }
}
