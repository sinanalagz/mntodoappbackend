package com.mn.todoapp.controller;

import com.mn.todoapp.dto.TodoSaveRequestDTO;
import com.mn.todoapp.dto.TodoUpdateRequestDTO;
import com.mn.todoapp.entity.Todo;
import com.mn.todoapp.manager.TodoManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin
public class MainController {
    private final TodoManager todoManager;

    @GetMapping
    private String hello() {
        return "hello";
    }

    @PostMapping("/save")
    @CrossOrigin
    private ResponseEntity<Todo> saveTodo(@RequestBody TodoSaveRequestDTO todo) {
        Todo savedTodo = todoManager.saveTodo(todo);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedTodo);
    }

    @GetMapping("/findAll")
    private ResponseEntity<List<Todo>> getTodos() {
        List<Todo> todos = todoManager.getTodos();
        return ResponseEntity.status(HttpStatus.OK).body(todos);
    }

    @PutMapping("/update")
    private ResponseEntity<Todo> updateTodo(@RequestBody TodoUpdateRequestDTO todoUpdateRequestDTO) {
        Todo updatedTodo = todoManager.updateTodo(todoUpdateRequestDTO);
        return ResponseEntity.status(HttpStatus.OK).body(updatedTodo);
    }
}
