package com.mn.todoapp.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TodoUpdateRequestDTO {
    private Long id;
    private boolean completed;
}