package com.mn.todoapp.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TodoSaveRequestDTO {
    private String task;
    private boolean completed;
}
