package com.mn.todoapp.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TodoSaveResponseDTO {
    private Long id;
    private String task;
    private boolean completed;
}
