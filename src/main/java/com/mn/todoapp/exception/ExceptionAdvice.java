package com.mn.todoapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(value = TodoNotFoundException.class)
    public ResponseEntity<Object> handleDBOperationsException(TodoNotFoundException e) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ErrorDetails errorDetails = new ErrorDetails(
                e.getMessage(),
                e,
                badRequest,
                LocalDateTime.now()
        );
        return new ResponseEntity<>(errorDetails, badRequest);
    }
}
