package com.mn.todoapp.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ErrorDetails {
    private final String message;
    private final Throwable throwable;
    private final HttpStatus status;
    private final LocalDateTime timestamp;
}
