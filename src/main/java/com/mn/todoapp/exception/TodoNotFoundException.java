package com.mn.todoapp.exception;

public class TodoNotFoundException extends RuntimeException{
    public TodoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TodoNotFoundException(String message) {
        super(message);
    }
}
