package com.mn.todoapp.service;

import com.mn.todoapp.entity.Todo;
import com.mn.todoapp.exception.TodoNotFoundException;
import com.mn.todoapp.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class TodoServiceTests {
    @Mock
    TodoRepository todoRepository;

    private TodoService todoService;


    @BeforeEach
    void initEach() {
        MockitoAnnotations.initMocks(this);
        todoService = new TodoServiceImpl(todoRepository);
    }

    @Test
    void givenTodo_whenSaveTodo_thenReturnSavedTodo() {
        Todo dummy_todo = Todo
                .builder()
                .task("Dummy Task")
                .completed(false)
                .build();
        Todo dummy_saved_todo = Todo
                .builder()
                .id(1L)
                .task("Dummy Task")
                .completed(false)
                .build();
        Mockito
                .when(todoRepository.save(Mockito.any(Todo.class)))
                .thenReturn(dummy_saved_todo);
        todoService.saveTodo(dummy_todo);
        Mockito
                .verify(todoRepository, Mockito.times(1))
                .save(Mockito.any(Todo.class));

    }

    @Test
    void noGiven_whenGetTodos_thenReturnTodos() {
        todoService.getTodos();
        Mockito
                .verify(todoRepository, Mockito.times(1))
                .findAll();
    }

    @Test
    void givenIdAndCompleted_whenUpdateTodo_thenReturnTodo() {
        Long dummyId = 1L;
        boolean dummyCompleted = false;
        Todo dummy_todo = Todo
                .builder()
                .id(1L)
                .task("Dummy Task")
                .completed(false)
                .build();
        Optional<Todo> dummy_todo_optional = Optional.ofNullable(dummy_todo);

        Mockito
                .when(todoRepository.findById(Mockito.anyLong()))
                .thenReturn(dummy_todo_optional);
        todoService.updateTodo(dummyId, dummyCompleted);
        Mockito
                .verify(todoRepository, Mockito.times(1))
                .save(Mockito.any(Todo.class));
    }

    @Test
    void givenIdAndCompleted_whenUpdateTodo_thenThrowException() {
        Long dummyId = 1L;
        boolean dummyCompleted = false;

        Optional<Todo> dummy_todo_optional = Optional.ofNullable(null);

        Mockito
                .when(todoRepository.findById(Mockito.anyLong()))
                .thenReturn(dummy_todo_optional);
        assertThrows(TodoNotFoundException.class, () -> todoService.updateTodo(dummyId, dummyCompleted));
    }
}
